
<a name="element"></a>

## element
Create a tag with a given name, attributes and children.
Elements that don't need closing tags are handled automatically.



| Param | Type | Description |
| --- | --- | --- |
| tag | <code>string</code> | The name of the tag |
| attributes | <code>Object</code> | A dict of attributes to add to the tag |
| ...children | <code>any</code> | Children to add inside the tag |

