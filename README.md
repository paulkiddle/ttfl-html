# TTFL HTML helpers

Helpers for generating html template without dictating which template tag function to use.

```javascript
import {element} from 'ttfl-html';

const link = (href, text) => element('a', { href }, text);

// Use with your favourite html tagging library
import html from 'encode-html-template-tag';

const example = link('//example.com', 'Click here for example.com')

example(html).render();
```

<a name="element"></a>

## element
Create a tag with a given name, attributes and children.
Elements that don't need closing tags are handled automatically.



| Param | Type | Description |
| --- | --- | --- |
| tag | <code>string</code> | The name of the tag |
| attributes | <code>Object</code> | A dict of attributes to add to the tag |
| ...children | <code>any</code> | Children to add inside the tag |

