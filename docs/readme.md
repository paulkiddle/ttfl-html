# TTFL HTML helpers

Helpers for generating html template without dictating which template tag function to use.

```javascript
import {element} from 'ttfl-html';

const link = (href, text) => element('a', { href }, text);

// Use with your favourite html tagging library
import html from 'encode-html-template-tag';

const example = link('//example.com', 'Click here for example.com')

example(html).render();
```
