import ttfl, {join} from 'ttfl';

function attribute([key, value]) {
	if(value == null || value === false) {
		return '';
	}

	if(value === true) {
		return ttfl` ${key}`;
	}

	if(typeof value === 'object' && !Array.isArray(value)) {
		value = Object.keys(value).filter(k=>value[k]);
	}

	if(Array.isArray(value)) {
		value = value.join(' ');
	}

	return ttfl` ${key}="${value}"`;
}

export const attrs = (...attrs) => {
	attrs = Object.assign({}, ...attrs.flat().filter(Boolean));

	const pairs = Object.entries(attrs).map(attribute);

	return join(pairs);
};

export const VOID_LIST = ['area', 'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'param', 'source', 'track', 'wbr'];
export const isVoid = tag => VOID_LIST.includes(tag.toLowerCase());

/**
 * Create a tag with a given name, attributes and children.
 * Elements that don't need closing tags are handled automatically.
 * @param {string} tag The name of the tag
 * @param {Object} attributes A dict of attributes to add to the tag
 * @param  {...any} children Children to add inside the tag
 */
export const element = (tag, attributes, ...children) => {
	const selfClosing = isVoid(tag);
	if(selfClosing && children.length > 0) {
		const e = new TypeError(`${tag} tag may not have any children, but ${children.length} children were provided`);
		e.children = children;
		throw e;
	}

	if(children.length === 1) {
		children = children[0];
	} else {
		children = join(children);
	}

	return 	selfClosing ? ttfl`<${tag}${attrs(attributes)} />` : ttfl`<${tag}${attrs(attributes)}>${children}</${tag}>`;
};
