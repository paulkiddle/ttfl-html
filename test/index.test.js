import { toString } from 'ttfl';
import { attrs, element } from '../src/index.js';

test('Renders attributes', ()=>{
	const tagged = attrs({
		checked: true,
		disabled: false,
		'data-value': 'value',
		undefined,
		class: {
			classA: true,
			classB: false,
			classC: true
		}
	});

	expect(tagged(toString)).toEqual(' checked data-value="value" class="classA classC"');
});

test('Renders elements', ()=>{
	const tagged = element('a', {
		href: '//www.example.com'
	}, 'Click me');

	expect(tagged(toString)).toEqual('<a href="//www.example.com">Click me</a>');
});

test('Renders void elements', ()=>{
	const tagged = element('hr', {
		class: 'divider'
	});

	expect(tagged(toString)).toEqual('<hr class="divider" />');
});

test('Throws void elements with children', ()=>{
	expect(()=> element('br', {}, 'etc')).toThrow();
});
